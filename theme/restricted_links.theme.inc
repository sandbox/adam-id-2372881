<?php
/**
 * @file Restricted_links.theme.inc. Theme related code for the restricted links module.
 */

/**
 * Themes the restricted links widget.
 */
function template_preprocess_restricted_widget(&$vars) {
  drupal_add_css(drupal_get_path('module', 'link') . '/link.css');

  $element = $vars['element'];
  // Prefix single value link fields with the name of the field.
  if (empty($element['#field']['multiple'])) {
    if (isset($element['url']) && !isset($element['title'])) {
      unset($element['url']['#title']);
    }
  }

  $content = '';
  $content .= '<div class="link-field-subrow">';
  if (isset($element['title'])) {
    $content .= '<div class="link-field-title link-field-column">' . drupal_render($element['title']) . '</div>';
  }
  $content .= '<div class="link-field-url' . (isset($element['title']) ? ' link-field-column' : '') . '">' . drupal_render($element['url']) . '</div>';
  $content .= '</div>';
  if (!empty($element['attributes']['target'])) {
    $content .= '<div class="link-attributes">' . drupal_render($element['attributes']['target']) . '</div>';
  }
  if (!empty($element['attributes']['title'])) {
    $content .= '<div class="link-attributes">' . drupal_render($element['attributes']['title']) . '</div>';
  }

  $content .= drupal_render($element['attributes']['restrictions']);

  return $content;
}

/**
 * Replaces theme_link_formatter_link_default() from link module.
 */
function template_preprocess_link_formatter_restricted_link_default(&$vars) {
  $link_options = $vars['element'];

  unset($link_options['restrictions']['rid']);
  unset($link_options['restrictions']['active_date']);
  unset($link_options['restrictions']['expires']);
  unset($link_options['restricted']['expire_date']);

  // Display a normal link if both title and URL are available.
  if (!empty($link_options['title']) && !empty($link_options['url'])) {
    $vars['content']['body']['#markup'] = l($link_options['title'], $link_options['url'], $link_options);
  }
  // If only a title, display the title.
  elseif (!empty($link_options['title'])) {
    $vars['content']['body']['#markup'] = check_plain($link_options['title']);
  }
  elseif (!empty($link_options['url'])) {
    $vars['content']['body']['#markup'] = l($link_options['title'], $link_options['url'], $link_options);
  }
}
