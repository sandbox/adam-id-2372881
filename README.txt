-- SUMMARY --

The Restricted Links module provides a new field display that allows users
to limit link fields to either specific user roles,
specific dates (activation and expiration), or a combination of both.

-- REQUIREMENTS --

This module requires the Link module.  If you want date restrictions,
the Date module is necessary.  This module has been tested with Drupal 7.


-- INSTALLATION --

* Install as usual, see 
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


-- CONFIGURATION --

* Configure link field display on new or existing field in 
Structure » Content Types » {Type} » Manage Display:

  - Find the link field, and under "Format" select "Title, as restricted link"

* When you create the node with the restricted link field :

  - Find the fieldset "Restricted Link Settings"
  - By default, anonymous and authenticated users can see links.
    Select other roles if you would like to change that setting.
  - By default, there is no active date and the links do not expire.
    If you have the Date module enabled, you must enter an activation date.
    To make the link expire, check the box and enter an expiration date.

-- TROUBLESHOOTING --

* If the link displays for an incorrect user, check the following:

  - Does the selected options match the appropriate roles?

* If the link does not display, check the following:

  - Does the selected options match the appropriate roles?

  - If the Date module is enabled, is there an active date?

* If the the link does not expire at the appropriate time:

  - Is the checkbox to set the link to expire checked?

  - Is there an expiration date set?


-- FAQ --

None yet.

-- CONTACT --

Current maintainers:
* Adam Murray (admurray) - http://drupal.org/user/2425502
